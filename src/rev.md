# Document version history
Different versions of the mechinical ventilator, latest version first.

## v0.1b:

> * Document history, Ventilator history added
> * UI for Prototype 1 added
> * Alarms list for P1 added

## v0.1a:

> * mdBook template initiated
> * Summary.md created
> * Introduction, Design philosophy added
