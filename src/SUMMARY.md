# Summary


[Introduction](./intro.md)
[Device version history](./devrev.md)

--------------------------------------------

* [Proto-1](./p1/p1.md)

  * [Specifications](./p1/specs.md)

  * [Operation Manual](./p1/opman/opman.md)
    * [Safety Information](./p1/opman/safetyop.md)
    * [Installations and Connections](./p1/opman/inc.md)
    * [User Interface](./p1/opman/ui.md)
    * [Operational modes](./p1/opman/modes.md)
    * [Alarms](./p1/opman/alarms.md)

  * [Build Manual](./p1/buildman/buildman.md)
    * [Ambu-Bag mechanism](./p1/buildman/ambubag.md)
    * [Components](./p1/buildman/components.md)
    * [Electrical](./p1/buildman/elec.md)
    * [Software](./p1/buildman/sw.md)
    * [Order of Assembly](./p1/buildman/ooa.md)
    * [BOM](./p1/buildman/bom.md)
    * [Accessories, Spares](./p1/buildman/acc.md)

  * [Design and Engineering](./p1/dne/dne.md)

* [Proto-2](./p2/p2.md)

* [Proto-3](./p3/p3.md)

* [--Production-release-v1--](./pr1/pr1.md)

* [Standards and References](./standards.md)



------------------------------------------------------------------
[Design Philosophy](./dp.md)
[Document version history](./rev.md)
[Glossary](./gloss.md)
[List of Illustrations](./loi.md)
[Acknowledgements](./ack.md)
[Postface](./postface.md)
