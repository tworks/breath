# Build Manual

Build Manual contains schematics, details and procedures to source, build, assemble and test the mechancal ventilator.

-----

The build manual is divided into the following sections which relate to major assemblies and sub-assemblies in the device.



* The Ambu-Bag mechanism
* Frame
  * Aluminium extrusion variant
  * Sheet metal body variant
* Gas circuits
  * Gas inlet circuits
  * Inspiratory circuit
  * Expiratory circuit
* Electrical schematics
* Electronics
  * Display board
  * Controller board
  * Oxygen sensor board
  * Pressure sensor boards (x2)
  * Valve board

## Releases

All schematics are released in these versioned bundles,
