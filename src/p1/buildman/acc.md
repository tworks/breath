# Accessories and Spares-list

List of spares to be packaged with each unit

| Part name | Qty | Part number in BOM |
| --------|------------|-------|
| Oxygen sensor | 1 | |
| Ambu-Bag | 1 | |
| Mechanical PEEP | |
