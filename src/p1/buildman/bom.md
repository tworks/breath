# BOM

| Sr. No. |          Part Name         |         Description         | Qty |
|:-------:|:--------------------------:|:---------------------------:|:---:|
|         |                            |                             |     |
|       1 | ATMEGA2560 Clone           | Microcontroller             |   2 |
|       2 | ADS1115                    | Aanlog to Digital convertor |   1 |
|       3 | FT232RL                    | FTDI interface              |   1 |
|       4 | 24LC256                    | EEPROM                      |   1 |
|       5 | ULN2803                    | Motor Driver                |   1 |
|       6 | MPXV5010DP                 | Pressure Sensor             |   4 |
|       7 | Pressure Limit switch      | Switch                      |   2 |
|       8 | 10k POT                    | Potentiometer               |   4 |
|       9 | LCD2004                    | Display                     |   1 |
|      10 | Resistors                  | Resistors                   |   2 |
|      11 | Diode-1N4148               | Diode                       |   1 |
|      12 | Diode-Schottkey            | Diode                       |   4 |
|      13 | FUSE                       | Fuse 5a 250V                |   1 |
|      14 | Transistor NPN             | Transistor                  |   5 |
|      15 | Inductive Proximity Sensor | -                           |   1 |
|      16 | FiO2 sensor                | Oxygen Sensor               |   1 |
|      17 | PCB Headers                | Connectors                  |  10 |
|      18 | Berg connectors            | Connectors                  |  10 |
|      19 | PSU                        | Power supply                |   1 |
|      20 | DC DC convertor            | Voltage Convertor           |   1 |
|      21 | TB6600                     | Stepper Driver              |   1 |
|      22 | TPST                       | Button                      |   1 |
|      23 | GT2 160mm                  | Belt                        |   1 |
|      24 | 8mm X 200mm                | Lead Screw                  |   1 |
|      25 | 8mm                        | Lead Screw nut              |   1 |
|      26 | NEMA 23                    | Motor                       |   1 |
|      27 | 200mm                      | Guide Rods                  |   1 |
|      28 | 8mm                        | Pillow block                |   2 |
|      29 | 8mm                        | Al Guide support            |   4 |
|      30 | 8mm                        | Rod carriage                |   2 |
|      31 | 500X500mm                  | Acrylic Sheet 6mm           |   1 |
|      32 | 250x250mm                  | Acrylic Sheet 3mm           |   1 |
|      33 | Al extrsuion 3030          | 7 meters                    |   1 |
|      34 | 30mm                       | L brackes                   |  50 |
|      35 | M6                         | T nuts                      |  50 |
|      36 | M6                         | Bolts                       |  50 |
|      37 | M5                         | Bolts                       |  50 |
|      38 | M4                         | Bolts                       |  50 |
|      39 | M3                         | Bolts                       |  50 |
|      40 | M4                         | Nuts                        |  50 |
|      41 | M3                         | Nuts                        |  50 |
|      42 | NO Solenoid Valve          | Valve                       |   1 |
|      43 | NC Solenoid Valve          | Valve                       |   3 |
|      44 | T Connectors               | Pnematic Conn.              |   2 |
|      45 | Bulkheads 0.5in            | Pnematic Conn.              |   4 |
|      46 | Bulkheads 0.25in           | Pnematic Conn.              |   4 |
|      47 | Needle Valve               | Pnematic Conn.              |   1 |
|      48 | Pressure Regulator         | Pnematic Reg.               |   2 |
|      49 | Pediatric HME              | Filter inhale               |   1 |
|      50 | Re/X700                    | Filter exhale               |   1 |
|      51 | PU tubing 5Meter           | Pipe / tube                 |   1 |
|      52 | Silicon tubing             | Pipe / tube                 |   1 |
|      53 | Ambu Bag Disposable        | Adult                       |   1 |
|      54 | Peep Valve                 | Valve                       |   1 |
