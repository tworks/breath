# Components and their significance  

## Medical

#### PEEP Vavle

The PEEP valve, where PEEP stands for Postitive End Expiratory Pressure is the pressure maintained in the patient's airway at end-exhalation. It prevents the lung from collapsing which increases oxygen concentration and reduces lung damage.
The PEEP valve typically has a range of 5-30 cm H<sub>2</sub>o
In early injury 5-10cmH<sub>2</sub>o

##### References
1. <http://www.specialoperationsmedicine.org/Documents/PFC%20WG/Why%20We%20Need%20PEEP%20Valves%20(Jan,%2014).pdf>
2. <https://www.youtube.com/watch?v=iuUSDR4ocCY> [  GRAPHIC |  NSFW  ]

----------

#### Bag valve mask  

A bag valve mask (BVM), sometimes known by the proprietary name Ambu bag or generically as a manual resuscitator or "self-inflating bag", is a hand-held device commonly used to provide positive pressure ventilation manually.

##### Reference

1. <https://en.wikipedia.org/wiki/Bag_valve_mask>

--------------

## Electromechanical

#### Diaphragm based solenoid valves (Normally Open)

Diaphragm based solenoid valves are used to open and close the inspiratory and expiratory circuits in this device. Normally open valves are used in the device so as to keep the circuits open in the case of power failure and diaphragm based solenoid valves don't transfer heat to the fluid involved.

##### Reference

1. <https://www.omega.ca/en/resources/valves-technical-principles>\

--------------

#### Pressure switches

A pressure switch is a form of switch that closes an electrical contact when a certain set fluid pressure has been reached on its input, we use this on both the hospital wall line and the cylinder line in the oxygen inlet circuit to prevent damage to the patient in case of accidental high pressures.

##### Reference

1. <https://en.wikipedia.org/wiki/Pressure_switch>

--------------

#### Plunger type solenoid valves

These type of solenoid valves are used in the oxygen inlet circuits, both on the cylinder and the wall line.

##### Reference

1. <https://www.directindustry.com/industrial-manufacturer/plunger-solenoid-valve-187313.html>

--------------

#### Pressure regulator

Pressure regulator with guage is used on the hospital wall line to control the amount of oxygen being bled into the bag-valve mask.

##### Reference

1. <https://en.wikipedia.org/wiki/Pressure_regulator>

--------------

#### Relief valves  

Relief valve automatically relieves the line from pressure if it exceeds a certain set pressure. This is to prevent damage to the patient's airway.

##### Reference

1. <https://en.wikipedia.org/wiki/Relief_valve>

--------------

## Sensors

#### Pressure Sensors

Pressure sensors are used in the inspiratory and expiratory circuit to measure the pressure.Pressure is an expression of the force required to stop a fluid from expanding, and is usually stated in terms of force per unit area. A pressure sensor usually acts as a transducer; it generates a signal as a function of the pressure imposed.

##### Reference

1. <https://en.wikipedia.org/wiki/Pressure_sensor>



--------------

#### Oxygen Sensors

We use the oxygen sensor in the inspiratory circuit to determine the Fraction of Inspired Oxygen or oxygen concentration.

##### Reference

1. <https://gaslab.com/blogs/articles/oxygen-sensors-ventilators-coronavirus>

--------------
#### Differential pressure sensors with venturi

Diferential pressure sensor in a venturi is known as a venturi flow meter. These are used in our inspiratory and expiratory circuits to determine the flow rates and volumes. It works by measuring the pressure drop (with a differential pressure sensor) that occurs between the entrance and the throat of the venturi.


##### Reference

1. <https://instrumentationtools.com/venturi-flow-meter-working-principle-animation/>

------------
