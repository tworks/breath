# Design and Engineering Manual

## General Operation

The mechinical ventilator ver.1 utilizes a Bag Valve a.k.a. an ambu-bag to create pressure in the patient's airway, and a system of valves, sesnors and plumbing (gas circuit) to achieve the following desired requirements of mechinical ventilation.

1. **Tidal Volume** - is the lung volume representing the normal voume of air displaced between inhalation and exhalation
2.**FiO<sub>2</sub>** - is the Fraction of Inspired Oxygen which means the percentage or concentration of oxygen in the inhaled air.
3. **PEEP** - stands for Positive End Expiratory pressure and is the positive pressure maintained in the patient's airway at hte end of exhalation by mechinical impedance.
4. **PIP** - stands for Peak Inspiratory Pressure and is the highest level of pressure applied to the lungs during inhalation.
5. **Respiratory Rate (bpm)** - is the number of breaths per minute.
6. **Inspiratory time:Expiratory time**
7. **Inspiratory pause**

## Gas circuit

The operation of the mechanical ventilator can be understood whilst following the gas circuit in the following sequence:

1. Oxygen inlet line
2. Inspiratory line
3. Expiratory line

![Gas Circuit](./gcrev5.png)

### Oxygen inlet line

Oxygen can be introduced in the system from 2 sources:
1. From the hospital infrastructure referred to as the **50 PSI hospital line** or a
2. From a **low-pressure O<sub>2</sub> cylinder**

**Pressure switches** in each line are used to determine if sufficient pressure is available in the respective line, the low-pressure cylinder line's pressure switch is caliberated at 10PSI and the hospital line is caliberated at 40PSI.
**Plunger-type solenoid valves** which are normally closed open till the desired percentage of Oxygen is detected by the Oxygen sensor at the inspiratory line. **A needle valve** is used to bleed the oxygen into the ambu-bag.

### Inspiratory line

Room air and oxygen enters the **ambu-bag** and proceeds to the manifold which contains a pressure sensor (PS1), an oxygen sensor and a relief valve. PS1 is used to measure the patient's airway pressure; the oxygen sensor is used to measure the oxygen percentage in the line and determine the FiO<sub>2</sub> (Fraction of Inspired Oxygen) and the relief valve is set to relieve any excess pressure that may accidentally build up in the line thus ensuring the patient's airway remains safe. The next component in the line is a venturi with a differential pressure sensor which is using the venturi principle to deterine Tidal volume delivered to the patient by the ventilator.
Normally Open(NO) diaphragm based solenoid valves are used to in both the inspiratort and expiratory lines to switch between inhalation and exhalation phases of the breath, they are normally open by design to enable patient's breathing in case of electrical failure of the ventilator. The inspiratory line continues through the Ventilator Port 1 (VP1) to the patient's lung.

### Expiratory Line  

The expiratory line begins from the patient's lung and a filter is used at the Ventilator Port 2 (VP2) to prevent contaminants entering from the patient's lungs into the room. Pressure Sensor 2 (PS2) is used to measure the airway pressure during expiration and a similar venturi as in the inspiratory port is used as a flowmeter. The expiratory line ends in a mechinical valve which is used to achieve the required PEEP (Positive End Expiratory pressure)
