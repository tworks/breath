
# Installations and Connections

The following connections have to be made for the operation of the device

* Power cord
* Oxygen source to device
  * Low pressure Oxygen cylinder; OR
  * 50 PSI wall line (hopsital oxygen line)
* Bag-valve mask (ambuBag)
  * Oxygen port to bag valve Mask
  * From ambuBag to device inlet (port labelled 'From ambuBag')
* Mechanical PEEP valve
* Patient circuit (endotracheal tube) to device
