# Operation Manual

<!-- This ventilator is designed for use by qualified and trained personnel under the direction of a physician and in accordance with applicable laws and regulations. -->

This Operator’s Manual contains detailed information and instructions for safe and effective set up, use and maintenance of the ventilator.

It contains the following:

* Ventilator Overview
* Installation and First time setupsudo apsudo apt updatesud
* Operation workflow
* Monitored Data (Status Screen)
* Ventilator Alarms
* Ventilator Checkout tests
* Operating Procedure
* Troubleshooting 
* Cleaning, Disinfecting and Sterilizing
* Maintenance

## Setup for first time use

## Installations and Connections

## Operation workflow

The following summary provides an overview of the operation of the ventilator system.

  1. Turn on the ventilator and perform a **Pre-use check**
  2. Perform a **Patient circuit test**
  3. Set the **Ventlation mode** (SIMV or )
  4. Check and adjust **Alarm profiles**
  5. Patient circuit connections
  5. ***Start ventilation***
  6. During ventilation, review and **adjust parameters** and settings if necessary
  7. Disconnect patient from the ventilator, when appropriate.

## Pre-use check

## Patient circuit tests

## Ventilation mode

## Alarm profiles

## Adjustable Parameter list

## Patient disconnection

Parameter | Description |
----------|------------|
*TV (Tidal Volume)* | desc |
*FiO<sub>2</sub>* | desc |
*PEEP* | desc |


<!-- ref = https://www.aarc.org/wp-content/uploads/2014/11/19802-001-F-LTV-1200-and-1150-Ops-Manual.pdf --!>
