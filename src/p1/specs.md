# Specifications




|   |  P1           |                                                                             |
|----|-----------------------------------------------|-----------------------------------------------------------------------------|
| | | |
| | | |
| | | |
| #  |  **Product Specifications**                      | **Achieved values**                                                             |
| 1  | Tidal volume                             | Range: 250-650 ml             |
| | | Accuracy: ± 25 ml |
| | | Settings: Increments of 50 ml|
| 2  | Respiratory rate (RR)                         | Range: 10-30 bpm                     |
| | | Accuracy: ± 0.5 bpm|
| | | Increments of 1 bpm |
| 3  | Inspiratory time: Expiratory time ratio (I:E) | Range: 1:1, 1:2, 1:3 3 fixed settings                                       |
| 4  | Positive End-Expiratory Pressure (PEEP) valve | Range: 5 – 30 cm H2O Setting: Adjustable in 1 cmH2O increments              |
| 5  | O2 Supply                                     | O2 supply through low pressure cylinder                                     |
| 6  | FiO2 sensor                                   | Range: 0.21 – 1.0                                                           |
| 7  | Peak Inspiratory Pressure (PIP)               | Range: 40 – 60 cm H2O;  Relief valve in inhalation Configurable by operator |
| 8  | Inspiratory pause                             | 20%                                                                         |
| 9  | Oxygen sensor                                 | Range: 0.21 – 1.0 FiO2 Accuracy: ± 0.01 FiO2                                |
| 10 | Modes                                         | Autonomous (Continuous Mandatory Ventilation - CMV)                         |
| | | |
| | | |
| | | |
| # | **Physical spec**     |              |
| 1 | Position          | Tabletop       |
| 2 | External Surfaces | Acrylic and SS |
| | | |
| | | |
| | | |
| # | **Sensor**                 |                                           |
| 1 | Airway Pressure Sensor | Connected at inspiratory and expiratory ports |
| 2 | FiO2 Sensor            | Integrated in Gas Circuit                     |
| | | |
| | | |
| | | |
| # | **UI features**                          | **Desc**                                                                                                         |
| 1 | User Interface Display              | 20x4 LCD display                                                                                                     |
|   |                                     | Capable of continuously displaying:                                                                                  |
|   |                                     | User settings for TV, RR, I:E, PIP, Ventilator output for PIP, PEEP, Inspiratory Pressure, Expiratory Pressure, FiO2 |
| 2 | User Interface Adjustable Variables | Respiratory Rate (BPM)                                                                                               |
|   |                                     | Tidal Volume (TV)                                                                                                    |
|   |                                     | PEEP (Mechanical PEEP valve)                                                                                         |
|   |                                     | Peak Inspiratory Pressure (PIP)(40-60 cm H2O)                                                                        |
| | | |
| | | |
| | | |
| # | **Electricals**       |         |
| 1 | Electrical Supply | 220V AC |
| | | |
| | | |
| | | |
| # | **Misc. parameters**  |                                                                                                                                                    |
| 1 | Easy to Use       | Requires less than 30 minutes of training for medical professional to operate                                                                          |
| 2 | Infection Control | Exhalation passes through HEPA filter                                                                                                                  |
|   |                   | All external surfaces are cleanable manually by wiping using an approved surface wipe with disinfectant or cloths and approved surface cleaning liquid |
|   |                   | Replaceable HEPA and HME filters to inspiratory circuit                                                                                                |
| | | |
| | | |
| | | |






<!-- <iframe width='100%' height='1500' src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSR9UZdDVC0w9EUbRfMqt5RPMbItDmtxUNmbX7Ncr84ettWNxK8WSE1ghFv6BXC9oApElHS55_iRrPx/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe> -->
