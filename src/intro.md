# Introduction

This is an Open Source Low-cost mechanical ventilator (henceforth referred to as 'device' or 'ventilator' or 'mechanical ventilator') designed  to address the current shortage of mechanical ventilators in the world caused due to the ongoing COVID19 pandemic. It is developed by **T-Works Foundation** (Government of Telangana) in collaboration with hardware, software and design professionals from the Hyderabad Hardware Community.

## Documentation overview

This document contains instructions to operate the device <!-- ([Operation Manual](./opman/opman.md))-->, instructions to manufacture the device <!--([Build Manual](./buildman/buildman.md))--> and design and engineering inforamtion organized into the progressive prototype and production ready versions of the device.

<pre>
            # Version 1 - Bag Valve Mask based
                    |__ Proto 1
                    |__ Proto 2
                    |__ Proto 3
                    |__ <bold>Production ready 1 </bold>

            # Version 2 - Compressor based
</pre>

## Purpose and scope

The scope of this device is limited to use in scenarios where commercial ventilators are not available due to the shortage created in the COVID-19 pandemic, it does not replace a commercial ventilator and it's capabilities.

## Partners

The following organisations are partners in this endevour, they contributed through provding manpower, insights, component sourcing and other forms of crucial support.

* **Honeywell**
* **Qualcomm**
* **NIMS**

## Team

This project was made possible from each of the following member's valuable contributions of time, expertise and effort.

| Name              | Organisation            |
|-------------------|-------------------------|
| Anand Rajagopalan | TWorks                  |
| Firoz Ahammad     | TWorks                  |
| Harshal Choudhary | Volunteer               |
| Kanishka Shah     | Entesla                 |
| Madhav Tenniti    | Spectrochem Instruments |
| Nagaraju          | Honeywell               |
| Rahul             | ConserVision            |
| Rajnikanth        | ConserVision            |
| Ravindra          | Qualcomm                |
| Rohan Rege        | TWorks                  |
| Shakeel Baig      | Qualcomm                |
| Sharat Reddy      | TWorks                  |
| Siddharth Arya    | TWorks                  |
| Simran Wasu       | TWorks                  |
| Srikanth Kumar    | TWorks                  |
| Sujai Karampuri   | TWorks                  |
| Surya Rao         | Althion                 |
| Anand harnoor     | Qualcomm                |
|                   | Qualcomm                |
| Viplov            | Trishula                |
| Karna Raj         | Trishula                |
| Anjan Babu        | TWorks                  |



*With special thanks to*
* Vinay (Bang design)

## Disclaimers

### Usage

The use of this ventilator should be limited to trained personnel, serious harm can be casued otherwise.

<!-- ### Limitation of usage

Whilst this device has been designed to be safe, it does not fully replace a commercially available FDA approved ventilator. This ventilator must be used in cases of last resort where no other equipment is available. -->

### Limitation of liabilities

While utmost care has been taken to accommodate patient and operator safety,
T-Works Foundation shall not be held responsible for any harm caused directly or indirectly by the use/misue of this ventilator.
